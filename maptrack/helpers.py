'''
© Copyright P. A. Arrutia Sota, F.M. Velotti 2019-2020. All rights not expressly granted are reserved 

Helper functions

'''

import fileinput
import matplotlib as mpl
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os
import pandas as pd
from scipy.optimize import minimize
from scipy.spatial import ConvexHull
from scipy.stats import norm
import shutil
import sys
import glob
import sympy as sp

import fast_track


def readtfs(filename, usecols=None, index_col=0, check_lossbug=True):
    header = {}
    nskip = 1
    closeit = False

    try:
        datafile = open(filename, 'r')
        closeit = True
    except TypeError:
        datafile = filename

    for line in datafile:
        nskip += 1
        if line.startswith('@'):
            entry = line.strip().split()
            header[entry[1]] = eval(' '.join(entry[3:]))
        elif line.startswith('*'):
            colnames = line.strip().split()[1:]
            break

    if closeit:
        datafile.close()

    table = pd.read_csv(filename, delim_whitespace = True,
                        skipinitialspace = True, skiprows = nskip,
                        names = colnames, usecols = usecols,
                        index_col = index_col)

    if check_lossbug:
        try:
            table['ELEMENT'] = table['ELEMENT'].apply(lambda x: str(x).split()[0])
        except KeyError:
            pass
        try:
            for location in table['ELEMENT'].unique():
                if not location.replace(".","").replace("_","").replace('$','').isalnum():
                    print("WARNING: some loss locations in "+filename+
                          " don't reduce to alphanumeric values. For example "+location)
                    break
                if location=="nan":
                    print("WARNING: some loss locations in "+filename+" are 'nan'.")
                    break
        except KeyError:
            pass

    return header, table


def replacer(filename,searchExp,replaceExp):
    """In-place replacement in text files.

    Finds all occurences of searchExp in the file and replaces each one
    with replaceExp, overwriting the original file.
    """
    for line in fileinput.input(filename, inplace=1):
        line = line.replace(searchExp,replaceExp)
        sys.stdout.write(line)


def getPTCmap(file_name):
    
    header, maptable = readtfs(file_name)
    maptable = {i: maptable[(maptable['N_VECTOR']==i)]
          for i in range(1, 7)}
    
    (x1, x2, x3, x4, x5, x6) = sp.symbols('x p_x y p_y t \delta')

    expressions = {}

    for i, submap in maptable.items():

        coeffs = {'c': submap['COEF'].values,
                  'exp': np.array(submap.loc[:, ['NX', 'NXP', 'NY', 'NYP', 'NDELTAP', 'NT']]),
                  }

        expression = 0
        for c, exp in zip(coeffs['c'], coeffs['exp']):

            if np.isclose(np.sum(exp),0): # PTC maps are applied around the CO
                print('Closed orbit in x_{} is {}.'.format(i, c))
                print('We remove it for tracking! Be careful :)')
                term = 0
            else:
                term = c*(x1**exp[0]*x2**exp[1]*x3**exp[2]*
                         x4**exp[3]*x6**exp[4]*x5**exp[5])   # Switch pt and t in the input to fit mad convetion!! Is there a minus sign?
            expression = sp.Add(expression, term)

        expressions[i] = expression

    expressions[5], expressions[6] = expressions[6], expressions[5] # Switch t and pt in the output

    coll_expressions = {i: sp.collect(sp.collect(sp.collect(sp.collect(sp.collect(ex, x6), x1), x2), x3), x4)
                                      for i, ex in expressions.items()}
    
    mymap = {i: sp.lambdify([x1, x2, x3, x4, x5, x6], express,
                       modules='numpy') 
               for i,express in coll_expressions.items()}
       
    return mymap, expressions


def quickZScheck(zs, particles):

    out = []

    n_p = particles.shape[1]

    for i in range(n_p):
        if particles[0, i] > zs:
            out.append(i)

    return np.delete(particles, np.s_[out], 1), out


def trackWithMap(particles_input, C_map, exps_map):

    particles_int = np.copy(particles_input)

    tmp_dpp = particles_int[5, :].copy()
    particles_int[5, :] = particles_int[4, :].copy()
    particles_int[4, :] = tmp_dpp

    particles_int = fast_track.trackNonLinearSinglePTC(C_map, exps_map, particles_int)

    tmp_dpp = particles_int[5, :].copy()
    particles_int[5, :] = particles_int[4, :].copy()
    particles_int[4, :] = tmp_dpp
    return particles_int


def trackPTC(particles, turns, zs, co_zs, mymaps, ignore_exps, turnByTurn=False):

    particles = np.copy(particles - co_zs)

    if not turnByTurn:
        for turn in range(turns):
            for mymap in mymaps:
                particles = fast_track.trackNonLinearSinglePTC(mymap, particles)

        return particles + co_zs

    else:
        particles_tot = np.zeros((particles.shape[0], particles.shape[1], turns))
        for turn in range(turns):
            particles_tot[:, :particles.shape[1], turn] = particles
            for mymap in mymaps:
                 particles = fast_track.trackNonLinearSinglePTC(mymap, particles)

        ignored_log = None # Backwards compatibility
        return particles_tot + co_zs.reshape(6,1,1), ignored_log

##############
# Benchmarking
##############

def get_jx_phix(xn, xpn,xn0=0.0,xpn0=0.0):
    jx = 0.5 * ((xn-xn0)** 2 + (xpn-xpn0)** 2)
    phix = np.arctan2((xpn-xpn0), (xn-xpn0))

    return jx,phix

def get_norm_x(x, xp, alfx, betx):
    xn = x / np.sqrt(betx)
    xpn = (x * alfx + xp * betx) / np.sqrt(betx)
    return xn, xpn

def my_diff(j1,j2):
    '''
    Relative difference of two actions j1,j2
    Avoid dividing by zero due to extracted particles getting coord 0
    '''
    diff = np.abs(j1-j2)
    deltaj = np.divide(diff, j1, out=np.zeros_like(j1), where=j1!=0.0)
    return deltaj.max(axis=1)

def benchmark(datadir,tracksdir=None,plot=True):
    '''Reads in from a simple_map generated folder and benchmarks the action error
    introduced by maptrack wrt tracked particles'''
    mapsdir = datadir +'/maps'
    if tracksdir is None: tracksdir = datadir+'/tracks'
    twissdir = datadir+'/twiss'

    #Load reference twiss/ptc_twiss files
    header_twiss_ref, data_twiss_ref = readtfs(twissdir+'/00000_twiss_ref.tfs')
    header_ptc_ref, data_ptc_ref = readtfs(twissdir+'/00000_ptc_twiss_ref.tfs')

    #Load closed-orbit
    x_co = data_ptc_ref['X'][0]
    px_co = data_ptc_ref['PX'][0]
    y_co = data_ptc_ref['Y'][0]
    py_co = data_ptc_ref['PY'][0]
    co_ptc = np.array([[x_co,px_co,y_co,py_co,0,0]]).T        

    #Create python maps from PTC files

    pymaps = {}

    for n,filename in enumerate(sorted(glob.glob(os.path.join(mapsdir, '*_ptc_map_sector*.tfs')))):
        pymaps[n] = getPTCmap(filename)

    track_df = pd.DataFrame()

    for n,filename in enumerate(sorted(glob.glob(os.path.join(tracksdir, 'trackout*')))): #input all tracks
        header_part, track_part = readtfs(filename)
        track_df = pd.concat([track_df,track_part]) #Make a df with all data


    #Grab the number of turns and number of particles to initialize numpy array
    nturns = int(track_df['TURN'].max())
    npar = int(track_df.index.max())
    #ndim=len(track_df.columns)-1 #The columns are the 6 coords + turn# (hopefully)
    ndim = 6 #TODO: Hardcoded dimensions

    particles_ptc = np.zeros((ndim,npar,nturns+1))

    for number, row in track_df.iterrows():
         particles_ptc[:,int(number)-1,int(row['TURN'])] = np.array([row['X'],row['PX'],
                                                                       row['Y'],row['PY'],
                                                                       row['T'],row['PT']])

    particles0 = particles_ptc[:,:,0]
    particles_map,_ = trackPTC(particles0, nturns+1, 68, co_ptc, [pymaps[key][0] for key in sorted(pymaps)],
                                             [pymaps[key][1] for key in sorted(pymaps)], turnByTurn=True)

    #Map_track does not have a full-aperture model
    #Therefore, if particle was lost in ptc, I make all the coords in maptrack 0 as well
    for part in track_df.index.unique():
        last_turn = track_df[track_df.index==part]['TURN'].max()
        particles_map[:,(part-1),(last_turn+1):]=0.0

    #TODO: Not clean. Take care of particles that blew up in maptrack
    particles_map = np.nan_to_num(particles_map)
    particles_map[particles_map>9999.0]=-9999.0
    particles_map[particles_map<-9999.0]=-9999.0

    #Calculation of action difference:
    x_map = particles_map[0,:,:]
    px_map = particles_map[1,:,:]
    pt_map = particles_map[5,:,:]
    x_ptc = particles_ptc[0,:,:]
    px_ptc = particles_ptc[1,:,:]
    pt_ptc = particles_ptc[5,:,:]

    obs_ele = data_twiss_ref.index[0] 

    #Dispersive orbit equilibrium (linear order only) to compute actions. TODO: non linear?

    x0_map = data_twiss_ref.loc[obs_ele]['DX']*pt_map
    px0_map = data_twiss_ref.loc[obs_ele]['DPX']*pt_map
    x0_ptc = data_twiss_ref.loc[obs_ele]['DX']*pt_ptc
    px0_ptc = data_twiss_ref.loc[obs_ele]['DPX']*pt_ptc

    xn_map,pxn_map = get_norm_x(x_map,px_map,data_ptc_ref['ALFX'][obs_ele],data_ptc_ref['BETX'][obs_ele])
    xn_ptc,pxn_ptc = get_norm_x(x_ptc,px_ptc,data_ptc_ref['ALFX'][obs_ele],data_ptc_ref['BETX'][obs_ele])

    xn0_map, xpn0_map = get_norm_x(x0_map,px0_map,data_ptc_ref['ALFX'][obs_ele],data_ptc_ref['BETX'][obs_ele])
    xn0_ptc, xpn0_ptc = get_norm_x(x0_ptc,px0_ptc,data_ptc_ref['ALFX'][obs_ele],data_ptc_ref['BETX'][obs_ele])

    jx_map, phix_map = get_jx_phix(xn_map,pxn_map,xn0=xn0_map,xpn0=xpn0_map)
    jx_ptc, phix_ptc = get_jx_phix(xn_ptc,pxn_ptc,xn0=xn0_ptc,xpn0=xpn0_ptc)

    jx_diff = my_diff(jx_ptc,jx_map)

    out_dict = {'djx': jx_diff, 
                'map': {'xn':xn_map, 'pxn':pxn_map, 'jx':jx_map, 'phix':phix_map},
                'ptc': {'xn':xn_ptc, 'pxn':pxn_ptc, 'jx':jx_ptc, 'phix':phix_ptc}
                }

    if plot: 

    #Plot the calculated quantities

        f1, (ax1,ax2) = plt.subplots(1,2,figsize=(15,10))

        ax1.plot(xn_map,pxn_map,'bo',label='maptrack')
        ax1.plot(xn_ptc,pxn_ptc,'r+',label='PTC_track')
        ax1.set_title('PXN vs XN')
        ax1.set_xlabel('xn')
        ax1.set_ylabel('pxn')

        ax2.plot(phix_map,jx_map,'bo')
        ax2.plot(phix_ptc,jx_ptc,'r+')
        ax2.set_title('JX vs PHIX')
        ax2.set_xlabel('phix')
        ax2.set_ylabel('jx')

        handles, labels = ax1.get_legend_handles_labels() #Hack to avoid duplicate labels
        by_label = dict(zip(labels, handles))
        f1.legend(by_label.values(), by_label.keys())
        #f1.savefig('{}/phase_space'.format(outputdir))

        f2,ax = plt.subplots()


        im = ax.scatter((x_map[:,0]-x0_map[:,0])*1000.0,pt_map[:,0]*1000.0,c=jx_diff,s=40,
            norm=LogNorm(vmin=jx_diff.min(), vmax=jx_diff.max()))
        ax.set_xlabel(r'x-$x_{0,disp}$ [mm]')
        ax.set_ylabel(r'PT [$10^{-3}$]')
        ax.set_title(r'Max $\Delta$ $J_x$ / $J_x$, turns={} (Initial distribution shown)'.format(nturns))

        #Set some nice axis limits
        x_max = (x_map[:,0]-x0_map[:,0]).max()*1000.0
        x_min = (x_map[:,0]-x0_map[:,0]).min()*1000.0
        pt_max = pt_map[:,0].max()*1000.0
        pt_min = pt_map[:,0].min()*1000.0

        ax.set_xlim([x_min-(x_max-x_min)/50.0,x_max+(x_max-x_min)/50.0])
        ax.set_ylim([pt_min-(pt_max-pt_min)/50.0,pt_max+(pt_max-pt_min)/50.0])
        f1.colorbar(im, ax=ax, label=r'$\Delta$ $J_x$ / $J_x$ ')
        f1.tight_layout()

    return out_dict