# MapTrack 

Module to 

1. Generate symplectic maps from madx PTC. (maptrack.simple_map)
2. Create python maps from PTC maps. (maptrack.fast_track)
3. Benchmark python maps against PTC tracking. (maptrack.helpers)
4. Track particles in python using Taylor maps. (maptrack.helpers)

Follow the tutorial in 'tutorial/ex.ipynb' to get started.

To install the package simply open the terminal in this directory and do 

```
pip install .
```

NOTE: this branch uses sympy to first symbolically compute and simplify the maps.
If you are looking for the cython branch look at the master branch.
