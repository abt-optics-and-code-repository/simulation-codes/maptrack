"""
© Copyright P. A. Arrutia Sota, F.M. Velottii 2019-2020. All rights not expressly granted are reserved 
"""
import os
import setuptools
from version import __version__

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="MapTrack",
    version=__version__,
    author="",
    author_email="",
    description="Package to generate, benchmark and use symplectic tracking maps in python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: ",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=[
        "numpy",
        "pandas==1.5.2",
        "scipy",
        "matplotlib",
        "sympy==1.12",
        "notebook",
    ],
)
