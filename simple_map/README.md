PTCmap
----------

What is it?

A module to generate PTC maps from a given lattice file. 
The PTC maps can be used as input for maptrack.py module to create symplectic maps for python.

PTCmap also allows the user to ptc_track a given distribution of particles in order to...
compare the results benchmark the maps generated from maptrack. 


