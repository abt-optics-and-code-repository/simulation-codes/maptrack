# -*- coding: utf-8 -*-
"""
Copyright P. A. Arrutia Sota, F.M. Velottii 2019-2020. All rights not expressly granted are reserved 

Module to use exported maps from PTC to track particles in python
@ author: P. A. Arrutia Sota
"""

import numpy as np


def trackNonLinearSinglePTC(mymap, particles):
    (particles[0],
     particles[1],
     particles[2],
     particles[3],
     particles[4],
     particles[5]) = (mymap[1](particles[0],particles[1],particles[2],particles[3],particles[4],particles[5]),
                    mymap[2](particles[0],particles[1],particles[2],particles[3],particles[4],particles[5]),
                   mymap[3](particles[0],particles[1],particles[2],particles[3],particles[4],particles[5]),
                   mymap[4](particles[0],particles[1],particles[2],particles[3],particles[4],particles[5]),
                   mymap[5](particles[0],particles[1],particles[2],particles[3],particles[4],particles[5]),
                   mymap[6](particles[0],particles[1],particles[2],particles[3],particles[4],particles[5])
                  )
    
    return particles


def quickZScheck(zs, width, particles):
    
    losses = 0
    extracted = 0

    to_be_deleted = []

    n_p = particles.shape[1]

    for i in range(n_p):
        if particles[0, i] > zs and particles[0, i] < (zs + width):
            losses += 1
            to_be_deleted.append(i)
        elif particles[0, i] > (zs + width):
            extracted += 1
            to_be_deleted.append(i)

    return np.delete(particles, np.s_[to_be_deleted], 1), losses, extracted


def checkAperture(x_max, xp_max, particles, delete=0):

    losses = 0
    extracted = 0

    to_be_deleted = []
    n_p = particles.shape[1]

    for i in range(n_p):
        if np.abs(particles[0, i]) > x_max or np.abs(particles[1, i]) > xp_max:
            losses += 1
            to_be_deleted.append(i)

    return np.delete(particles, np.s_[to_be_deleted], 1), losses



