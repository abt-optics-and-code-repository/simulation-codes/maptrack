"""
© Copyright F.M. Velotti 2019-2020. All rights not expressly granted are reserved
"""
from distutils.core import setup
from Cython.Build import cythonize

setup(name='fast_track', ext_modules=cythonize('fast_track.pyx'),)
