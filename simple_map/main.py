# -*- coding: utf-8 -*-
"""
© Copyright P. A. Arrutia Sota, F.M. Velottii 2019-2020. All rights not expressly granted are reserved 

Symplectic map generation from PTC/MADX + benchmarking of maps

Modifies mapping/tracking template and outputs maps/tracks

Tested to work on MADX 5.02.06, 5.02.08, 5.05.02

@author: Pablo Arrutia
"""

import os
import stat
import sys
import fileinput
import subprocess
from shutil import copyfile
import scipy.stats as stats
import pandas as pd
import numpy as np
import warnings
import pickle
import pathlib



class Settings:
    """Settings for the PTC mappin/tracking"""
    def __init__(self, name, outputdir=None, studygroup='', disk=None,debug=False):
        if outputdir is None:
            if not disk in ['afsprivate', 'afspublic', 'afsproject']:
                print("Disk setting not valid. Valid options are afsprivate'"+
                      "/'afspublic'/'afsproject'. Defaulting to afsprivate."+
                      "(EOS is not yet supported.)")
            user = os.environ["USER"]
            if disk=='afsproject':
                outputdir = '/afs/cern.ch/project/sloex/'+studygroup
                if not os.path.exists(outputdir):
                    print("ERROR: To submit via afsproject a valid studygroup must be given. "+
                          "Take a look at the folder structure in /afs/cern.ch/project/sloex "+
                          "to find an appropriate one or contact the admins to make one. Exiting.")
                    exit()
            elif disk=='afspublic':
                outputdir = '/afs/cern.ch/work/'+user[0]+'/'+user+'/public/madxBatchData/'+studygroup
            else:
                outputdir = '/afs/cern.ch/work/'+user[0]+'/'+user+'/private/madxBatchData/'+studygroup
        else:
            if disk is not None:
                print("Disk setting is not used because outputdir was specified.")

        self.name = name
        self.datadir = outputdir+"/"+name+"/"
        #self.home = '/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1])
        self.madxversion = 'madx/executable/location/here'

        self.template = str(pathlib.Path(__file__).parent.absolute()) + '/madx/get_maps_template.madx'
        self.seqfile = 'dumped/sequence/file/location/here' 
        self.myreplace = {}

        self.seqname = '<Sequence_name>'
        self.sectors = [('MR$START','MR$END')] #Sectors to create maps of
        self.dpp = 0 #momentum offset of the closed orbit
        self.pc = 1.000 #momentum of the beam
        self.particle = 'proton'

        self.PTCkwargs = {'pyMAP_ORDER':'3', #Order of non-linearity
                'pyMODEL_PTC':'2', #1 = drift-kick-drift, 2 = matrix-kick-matrix, 3 = Delta-matrix-kick-matrix (SixTrack)
                'pyMETHOD_PTC':'6', #integration order: 2,4,6
                'pySLICE_NUMBER':'5', #Name of sequence
                'pyICASE_PTC':'5'} #Phase space dimensions: 4,5,6}

        self.mapping = False


        self.tracking = False 
        self.trackkwargs = {'pyNTURNS':'10',  #Number of tracked turns
                                'pyNORM_NO':'15'} #Norm of the ptc_tracking routine.
                                
        self.initialconditions = [{'x':i*10.5e-3/50.0,
                                'px':0,
                                'y':0,
                                'py':0,
                                't':0,
                                'pt':0} for i in range(50)] #Initial conditions for tracking/benchmarking

        self._custom_datadir = False
        self.run_madx=True

        self.debug = debug #Debugging mode on/off

    def set_name(self, name):
        self.name = name
        if not self._custom_datadir:
            outputdir = '/'.join(self.datadir.split('/')[:-2])
            self.datadir = outputdir+'/'+name+"/"
        
    def set_studygroup(self, studygroup):
        if not self._custom_datadir:
            temp = self.datadir.split('/')
            temp[-3] = studygroup
            self.datadir = '/'.join(temp)+'/'

    def set_datadir(self, direc):
        self.datadir = direc
        self._custom_datadir = True
        
    def set_params(self, sectors=None, seqname=None, seqfile=None):
        if sectors is not None:
            self.sectors = sectors
        if seqname is not None:
            self.seqname = seqname
        if seqfile is not None:
            self.seqfile = seqfile


def replacer(filename,searchExp,replaceExp):
    """In-place replacement in text files.

    Finds all occurences of searchExp in the file and replaces each one
    with replaceExp, overwriting the original file.
    """
    for line in fileinput.input(filename, inplace=1):
        line = line.replace(searchExp,replaceExp)
        sys.stdout.write(line)

def readtfs(filename, usecols=None, index_col=0, check_lossbug=True):
    header = {}
    nskip = 1
    closeit = False

    try:
        datafile = open(filename, 'r')
        closeit = True
    except TypeError:
        datafile = filename

    for line in datafile:
        nskip += 1
        if line.startswith('@'):
            entry = line.strip().split()
            header[entry[1]] = eval(' '.join(entry[3:]))
        elif line.startswith('*'):
            colnames = line.strip().split()[1:]
            break

    if closeit:
        datafile.close()

    table = pd.read_csv(filename, delim_whitespace = True,
                        skipinitialspace = True, skiprows = nskip,
                        names = colnames, usecols = usecols,
                        index_col = index_col)

    if check_lossbug:
        try:
            table['ELEMENT'] = table['ELEMENT'].apply(lambda x: str(x).split()[0])
        except KeyError:
            pass
        try:
            for location in table['ELEMENT'].unique():
                if not location.replace(".","").replace("_","").replace('$','').isalnum():
                    print("WARNING: some loss locations in "+filename+
                          " don't reduce to alphanumeric values. For example "+location)
                    break
                if location=="nan":
                    print("WARNING: some loss locations in "+filename+" are 'nan'.")
                    break
        except KeyError:
            pass

    return header, table

def get_L_from_ele(seqfile,seqname,elements,madxversion):
    '''Find lengths of list of elements in a sequence. NOT USED'''

    line=('call, file = {};\n'.format(seqfile)+
        'BEAM;\n'+
        'use, sequence = {};\n'.format(seqname)+
        'select, flag=twiss, clear;\n'+
        'select, flag=twiss, column=name,l;\n'+
        'twiss;\n'+
        'write, table=twiss, file="./get_l.tfs";\n'+
        'EXIT;')

    with open('./get_l.madx','w') as f:
        f.write(line)

    madxCommand = '{} ./get_l.madx'.format(madxversion)
    process = subprocess.Popen(madxCommand.split(), stdout=subprocess.PIPE)
    out,err = process.communicate()

    _,table = readtfs('./get_l.tfs')

    lengths = {}
    for ele in elements:
        try: lengths[ele]=table.loc[ele]['L']
        except: lengths[ele]='NOTFOUND'

    os.remove('./get_l.madx')
    os.remove('./get_l.tfs')

    return lengths

def run_job(settings):
    """Runs jobs for simulation defined by settings."""

    if settings.debug:
        warnings.warn("DEBUGGING MODE is ON")

    starting_dir = os.getcwd()

    if os.path.exists(settings.datadir):
        settings.datadir = settings.datadir[:-1]+'_/'
        i = 1
        while True:
            i += 1
            if not os.path.exists(settings.datadir[:-1]+str(i)+"/"):
                break
        settings.datadir = settings.datadir[:-1]+str(i)+"/"
    os.makedirs(settings.datadir)
    os.chdir(settings.datadir)

    with open("settings.info", 'w') as f:
        f.write("These files were generated using simple_map.\n\n")
        f.write("Settings used:\n")
        for item in vars(settings).items():
            f.write(str(item[0])+" = "+str(item[1]).replace('\n','')+"\n")

    with open('settings.pickle','wb') as f:
        pickle.dump(settings,f)

    #---------------------------
    #Fill in get_map_template data
    #---------------------------

    copyfile(settings.template, "main.madx")

    #---------------------------------------------------
    #  Set sector boundaries with markers and install them
    #----------------------------------------------------

    line=''

    for n in range(len(settings.sectors)):
        line+='SECTOR{}_START:MARKER;\n'.format(n)
        line+='SECTOR{}_END:MARKER;\n\n'.format(n)

    line+='SEQEDIT, sequence=pySEQ;\n'
    line+='FLATTEN;\n'

    for n,sector in enumerate(settings.sectors):
        line+='INSTALL, ELEMENT = SECTOR{}_START, at =-{}->L/2, FROM ={};\n'.format(n,sector[0],sector[0])
        line+='INSTALL, ELEMENT = SECTOR{}_END, at =-{}->L/2, FROM ={};\n'.format(n,sector[1],sector[1])

    line+='FLATTEN;\n'
    line+='ENDEDIT;\n'

    replacer("main.madx",'/*pyMARKERS*/',line)

    replacer("main.madx",'pySEQ',settings.seqname)
    replacer("main.madx",'pyDPP',str(settings.dpp))
    replacer("main.madx",'pyPC_BEAM',str(settings.pc))
    replacer("main.madx",'pyPARTICLE',str(settings.particle))

    for key,replacement in settings.PTCkwargs.items():
        replacer("main.madx", key, replacement)

    if settings.mapping:
        line=''
        for n in range(len(settings.sectors)):
            line += 'exec, get_sector_map_ptc(SECTOR{}_START,SECTOR{}_END,{:05d});\n'.format(n,n,n)
        replacer("main.madx", '/*pyMAPPING*/', line)

    if settings.tracking:
        for key,replacement in settings.trackkwargs.items():
            replacer("main.madx",key,replacement)

        replacer("main.madx",'/*pyTRACKING','')
        replacer("main.madx",'pyTRACKING*/','')

        line=''
        for ic in settings.initialconditions:
            line += 'ptc_start, x={} + x_co, px={} + px_co, y={} + y_co, py={} + py_co, t={}, pt={};\n '.format(
                 ic['x'],ic['px'],ic['y'],ic['py'],ic['t'],ic['pt'])

        replacer("main.madx",'pyINITIALCONDITIONS',line)


    replacer("main.madx", 'pyDATADIR', settings.datadir)
    replacer("main.madx", 'pySSEQFILE', settings.seqfile)

    for key, replacement in settings.myreplace.items():
        replacer("main.madx", key, replacement)

    print('Executable is ready...')

    os.mkdir("twiss")
    os.mkdir("maps")
    os.mkdir("tracks")

    print('Output Directories created...')


    #Run main.madx to generate output files
    if settings.run_madx:

        madxCommand = '{} main.madx'.format(settings.madxversion)

        process = subprocess.Popen(madxCommand.split(), stdout=subprocess.PIPE)
        out,err = process.communicate()

        with open('mapping.out','wb') as log:
            log.write(out)

        #After job cleanup/diagnostics

        madx_del("./main.madx",attr=[True],keep=settings.debug)
        madx_del("./fort.18",attr=[settings.mapping],keep=settings.debug)
        madx_del("./Maxwellian_bend_for_ptc.txt",
            attr=[False],
            keep=settings.debug) #this file is not generated in new madx versions anymore

        if settings.debug: 
            #Under construction
            print('Looking for MADX errors/warnings... (Debug ON)')

            with open('mapping.out') as log:
                warn_lines = [line for line in log.readlines() if 'warning' in line]

            with open('mapping.out') as log:
                fatal_lines = [line for line in log.readlines() if 'fatal +++' in line]

            print('MADX warnings: \n')
            for line in warn_lines:
                print(line)
            print('MADX fatal errors:\n')
            for line in fatal_lines:
                print(line)


    os.chdir(starting_dir)

    print('Job Finished!')

#Output files handling functions

def madx_del(filename,attr,keep):
    '''Deals with secondary madx output files'''
    if (True in attr) and (not os.path.exists(filename)):
        warnings.warn('{} should have been created, but it was not'.format(filename))

    if (not keep) and (os.path.exists(filename)):
        os.remove(filename)



