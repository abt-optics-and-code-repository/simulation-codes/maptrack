# MapTrack 

Functions to track particles in python using:
 - PTC maps (obtained from PTC MADX interface
 - M and T matrix


### To compile the module run (python 2.7):

''''
python setup.py build_ext --inplace
''''