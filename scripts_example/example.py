# -*- coding: utf-8 -*-
'''
Example script to use simple_map module 

I generate PTC maps for the MedAustron machine and track some particles for benchmarking

@author: Pablo Arrutia
'''

#TODO: getting error ERROR: AD sub zero length vector, but still running. Not from madx or ptc
#TODO: getting madx error +++ memory access outside program range. It is not reproducible. Happens sometimes


from shutil import copyfile
import subprocess
import sys
import os
import time

sys.path.insert(1, '../')
import simple_map as sm
import helpers as hs



#--------------------------
#      Menu:
#--------------------------

#Directory setup 

outputdir = '/directory/for/job/output' #All generated files are dumped here

maptemplate = '../simple_map/madx/get_maps_template.madx' #PTC mapping/tracking template. Comes with simple_map module

madxversion = 'location/of/madx/executable'

seqfile = 'location/of/sequence/file/executed/before/mapping' #Example can be found in /simple_map/madx/seqfile_template.madx

settings = sm.Settings('helloworld',outputdir=outputdir,debug=True)


settings.madxversion = madxversion
settings.template = maptemplate
settings.seqfile = seqfile

settings.myreplace={'/*pyPREMAPPING*/':'',
'/*pyPRETRACKING*/':'',
'/*pyPOSTTRACKING*/':''} #Some standard placeholders already in the template

settings.seqname='mr'
settings.pc = 0.25
settings.sectors = [('<SECTOR0_START_ELE>','SECTOR0_END_ELE'),('SECTOR1_START_ELE','SECTOR1_END_ELE')]
#NOTE: sectors is a list of tuples with (START_SECTOR_ELE, END_SECTOR_ELE). 
#... PTCmap will install markers BEFORE these elements.
#NOTE: Try to stay periodic (sectors[0][0]=sectors[-1][1]) and continous (sectors[n][1]=sectors[n+1][1])...
#... This will make benchmarking easy. Break these conditions at your own risk!!

settings.PTCkwargs ={'pyMAP_ORDER':'3', #Order of non-linearity
                'pyMODEL_PTC':'2', #1 = drift-kick-drift, 2 = matrix-kick-matrix, 3 = Delta-matrix-kick-matrix (SixTrack)
                'pyMETHOD_PTC':'6', #integration order: 2,4,6
                'pySLICE_NUMBER':'5', #Name of sequence
                'pyICASE_PTC':'5'} #Phase space dimensions: 4,5,6}

settings.mapping = True #Generate PTCmaps?
settings.tracking = False#Track benchmarking particles?
settings.trackkwargs = {'pyNTURNS':'10',  #Number of tracked turns
                        'pyNORM_NO':'15'} #Norm of the ptc_tracking routine.
                        
settings.initialconditions = [{'x':i*10.5e-3/50.0,
                        'px':0,
                        'y':0,
                        'py':0,
                        't':0,
                        'pt':0} for i in range(50)] #Initial conditions for tracking/benchmarking particles

settings.run_madx=True

sm.run_job(settings)














